from django.shortcuts import render
from django.http import HttpResponse
from bs4 import BeautifulSoup
import pandas as pd

def data(request):
    labels = ['D/H Miles', 'Trip Miles', 'Origin', 'ST', 'Destination', 'ST',  'Trailer Type', 'Load Size', 'Length', 'Weight', 'Payrate', 'Est Rate Per Mile', 'Ship Date', 'Quick Pay', 'Credit Report', 'TIA', 'Company']
    data = []
    if request.method == 'POST' and request.FILES.get('htmlFile'):
        html_file = request.FILES['htmlFile']
        
        # Read the uploaded HTML file
        html_content = html_file.read().decode('utf-8')

        # Create a BeautifulSoup object
        soup = BeautifulSoup(html_content, 'html.parser')

        # Find elements and extract data
        data = []

        # Example: Scraping all <tr> elements with class "nl-load-row"
        loads = soup.find_all('tr', class_="tr-main-result")
        for load in loads:
            values = load.find_all('td')
            values = [v.text.replace(" ", "").replace("\n", "") for v in values]
            values.remove(values[1])
            values.remove(values[-1])
            data.append(values[1:]) 
        df = pd.DataFrame(data, columns=labels)

        # Save DataFrame as an Excel file with labels
        output_file = 'loads.xlsx'
        df.to_excel(output_file, index=False)

        with open(output_file, 'rb') as file:
            response = HttpResponse(file.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment; filename="loads.xlsx"'

        # Return the response for file download
        return response

    else:
        return render(request, 'scraping.html')
